import kotlinx.coroutines.*
import kotlin.random.Random

suspend fun First():Int
{
    delay(1000L)
    return Random.nextInt(0,15)

}
suspend fun Second():Int
{
    delay(1500L)
    return Random.nextInt(0,15)
}

suspend fun main()
{
    val begin = System.currentTimeMillis();
    val result = First() + Second()
    val end = System.currentTimeMillis();
    println("Result is $result. Elapsed time in milliseconds: ${end-begin}")
}

