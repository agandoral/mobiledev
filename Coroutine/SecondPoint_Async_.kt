import kotlinx.coroutines.*
import kotlin.random.Random

suspend fun First():Int
{
    delay(1000L)
    return Random.nextInt(0,15)

}
suspend fun Second():Int
{
    delay(1500L)
    return Random.nextInt(0,15)
}

suspend fun main() = coroutineScope{
    val begin = System.currentTimeMillis();

    val Firstmessage: Deferred<Int> = async {First()}
    val Secondmessage: Deferred<Int> = async{Second()}
    val result = Firstmessage.await() + Secondmessage.await()
    val end = System.currentTimeMillis();
    println("Result is $result. Elapsed time in milliseconds: ${end-begin}")
}